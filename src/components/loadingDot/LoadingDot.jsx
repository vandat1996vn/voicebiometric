import React from 'react';
import styled from 'styled-components';

function LoadingDot({text=''}) {
  return (
    <Container>
      <div className='loading-dots'>
        <p>{text}</p>
        <div>
          <span></span>
          <span></span>
          <span></span>
        </div>
      </div>
    </Container>
  );
}
const Container = styled.div`
  display: flex;
  justify-content: center;
  top: 0px;
  left: 0px;
  width: 100vw;
  height: 100vh;
  position: fixed;
  z-index: 100;
  /* margin-top: 50px; */
  .loading-dots {
    position: absolute;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    width: 100%;
    background: rgba(0, 0, 0, 0.5);
    height: 100%;
    border-radius: 6px;
  }
  p{
    color: #fff;
  }

  .loading-dots div span {
    display: inline-block;
    margin: 0 5px;
    width: 8px;
    height: 8px;
    border-radius: 50%;
    background-color: #fff;
    animation: loading-dots-animation 1s ease-in-out infinite;
  }

  .loading-dots div span:nth-child(1) {
    animation-delay: 0s;
  }

  .loading-dots div span:nth-child(2) {
    animation-delay: 0.2s;
  }

  .loading-dots div span:nth-child(3) {
    animation-delay: 0.4s;
  }

  @keyframes loading-dots-animation {
    0% {
      transform: scale(1);
    }
    50% {
      transform: scale(1.5);
    }
    100% {
      transform: scale(1);
    }
  }
`;

export default LoadingDot;
