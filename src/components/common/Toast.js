import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { resetState } from '../../features/AlertSlice';
import { SuccessIcon } from '../icon/SuccessIcon';
import { CloseIcon } from '../icon/CloseIcon';
import { IoIosWarning } from 'react-icons/io';

const Toast = () => {
  const dispatch = useDispatch();
  const alert = useSelector((state) => state.alertSlice);
  useEffect(() => {
    setTimeout(() => {
      dispatch(resetState());
    }, 5000);
  }, [alert.showStatus]);

  const bgStatus = (type) => {
    if (alert.type === 'success') {
      return `bg-success`;
    }
    if (alert.type === 'error') {
      return `bg-error`;
    }
    if (alert.type === 'warning') {
      return `bg-warning`;
    }
  };

  return (
    <>
      {alert.showStatus && (
        <div className={`notifi custom-toast  ${bgStatus(alert.type)}`} 
        style={{ zIndex: '5000' , position:'absolute' , left:'10px' , top:'10px' }}>
          <div className='notification-image'>
            {alert.type === 'success' && <SuccessIcon />}
            {alert.type === 'error' && <CloseIcon />}
            {alert.type === 'warning' && <IoIosWarning />}
          </div>
          <div>
            <p className='notification-title'>{alert.type === 'success' ? 'Thành công' : 'Lỗi'}</p>
            <p className='notification-message'>{alert.message}</p>
          </div>
        </div>
      )}
    </>
  );
};

export default Toast;
