import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import { faceOcr } from '../api/faceOcr'

export const compareOcr = createAsyncThunk('compare-face', async (payload, { rejectWithValue }) => {
  try {
    console.log('payload' ,payload )
    const dataURItoBlob = (dataURI) => {
      const byteString = atob(dataURI.split(',')[1]);
      const mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
      const ab = new ArrayBuffer(byteString.length);
      const ia = new Uint8Array(ab);
      for (let i = 0; i < byteString.length; i++) {
        ia[i] = byteString.charCodeAt(i);
      }
      const blob = new Blob([ab], { type: mimeString });
      return blob;
    }
    const blobImage1 = dataURItoBlob(payload.front);
    const blobImage2 = dataURItoBlob(payload.back);
    const formData = new FormData();
    formData.append('front', blobImage1);
    formData.append('back', blobImage2);
    const response = await faceOcr.compareFaceOcr(formData)

    return response.data;
  } catch (error) {
    return rejectWithValue(error.response);
  }
});
const VerifyOcr = createSlice({
  name: "face",
  initialState: {
    img1: '',
    img2: '',
    resultPercent:'',
    data:''
  },
  reducers: {
    addImgOneOcr: (state, actions) => {
      state.img1 = actions.payload
    },
    addImgTwoOcr: (state, actions) => {
      state.img2 = actions.payload
    },
    addPercentOcr: (state, actions) => {
      state.resultPercent = actions.payload
    },
    addDataOcr: (state, actions) => {
      state.data = actions.payload
    }
  },
  extraReducers: {},
});

const { actions, reducer } = VerifyOcr;
export const { addImgOneOcr, addImgTwoOcr , addPercentOcr , addDataOcr } = actions
export default reducer;
