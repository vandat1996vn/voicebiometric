import { createSlice , createAsyncThunk } from "@reduxjs/toolkit";
import {voiceApi} from '../api/voiceApi'

const initialState = {
  loading: false,
  audioFirst: null,
  audioSecond: null,
  resultVoice:{},
};

export const compareVoice = createAsyncThunk('compare-voice', async (payload, { rejectWithValue }) => {
  try {
    const response = await voiceApi.compareVoice(payload)

    return response.data;
  } catch (error) {
    return rejectWithValue(error.response);
  }
});

const VerifyVoice = createSlice({
  name: 'voice',
  initialState: initialState,
  reducers: {
    voiceRecordOne: (state, action) => {
      state.audioFirst = action?.payload;
    },
    voiceRecordTwo: (state, action) => {
      state.audioSecond = action?.payload;
    },
    voiceResultAction: (state, action) => {
      state.resultVoice = action?.payload;
    },
  }
});

const { actions, reducer } = VerifyVoice;

export const { voiceRecordOne , voiceRecordTwo , voiceResultAction} = actions
export const selectVoiceOne = (state) => state.verifyVoice.audioFirst;
export const selectVoiceTwo = (state) => state.verifyVoice.audioSecond;
export const selectVoiceResult = (state) => state.verifyVoice.resultVoice;
export default reducer;