import React, { useEffect, useState, useRef } from "react";
import styled from "styled-components";
import btnVoice from "../../../../assets/images/btn-take-photo.png";
import paternImg from "../../../../assets/images/cccd-front.jpg";
import { BsFillArrowRightCircleFill } from "react-icons/bs";
import logovntelMini from "../../../../assets/images/logoMini.png";
import { useDispatch, useSelector } from "react-redux";
import { addImgOneOcr } from "../../../../features/VerifyOcr";
import { HiArrowPath } from "react-icons/hi2";
import Webcam from "react-webcam";
import html2canvas from "html2canvas";
import Toast from "../../../../components/common/Toast";
import {
  showMessageSuccess,
  showMessageError,
} from "../../../../features/AlertSlice";
const FACING_MODE_USER = "user";
const FACING_MODE_ENVIRONMENT = "environment";

function SnapShotOneNew2({ setContinueImg2, setClassAdd }) {
  // const img1 = useSelector((state) => state.verifyFace.img1);
  const dispatch = useDispatch();
  const [camera, setCamera] = useState(false);
  const [captureImg, setCaptureImg] = useState(false);
  // ======================
  const webcamRef = useRef(null);
  const [isStreaming, setIsStreaming] = useState(false);
  const [imgSrc, setImgSrc] = useState(null);
  const videoConstraints = {
    facingMode: FACING_MODE_ENVIRONMENT,
  };
  const [facingMode, setFacingMode] = React.useState(FACING_MODE_ENVIRONMENT);
  const handleClick = React.useCallback(() => {
    setFacingMode((prevState) =>
      prevState === FACING_MODE_USER
        ? FACING_MODE_ENVIRONMENT
        : FACING_MODE_USER
    );
  }, []);
  const handleStartStream = () => {
    setIsStreaming(true);
    setCamera(true);
  };

  const handleStopStream = () => {
    setIsStreaming(false);
  };
  const handleTakePhoto = () => {
    const imgData = webcamRef.current.getScreenshot();
    setImgSrc(imgData);
  };
  const handleTakePhotoBack = () => {
    setImgSrc("");
    setCaptureImg(false);
  };
  let stream = null;

  // const turnOffWebcam = () => {
  //   navigator.mediaDevices
  //     .getUserMedia({ video: true })
  //     .then(function (mediaStream) {
  //       stream = mediaStream;
  //       document.querySelector("video").srcObject = stream;
  //       stream.getTracks().forEach(function (track) {
  //         track.stop();
  //       });
  //     });
  // };
  useEffect(() => {
    if (imgSrc) {
      dispatch(addImgOneOcr(imgSrc));
      setCaptureImg(true);
      // alert("chup anh thanh cong ");
      dispatch(showMessageSuccess());
      // turnOffWebcam();
    }
    // return () => {
    //   turnOffWebcam();
    // };
    setClassAdd(true);
  }, [imgSrc]);
  console.log("webcamRef", webcamRef);
  return (
    <>
      <Container>
        <div className="content">
          <h1 className="title-big">OCR</h1>
          <h3>Hệ thống OCR</h3>
          <div className="spin__number">
            <div className="spin__main">
              <div className="title">
                <h3>Chụp ảnh mặt trước</h3>
              </div>
              <div className="patern__img">
                {isStreaming ? (
                  <div
                    id="webcam-container"
                    style={{
                      position: "relative",
                      width: "460px",
                      height: "240px",
                      zIndex: "1000",
                    }}
                  >
                    {imgSrc ? (
                      <img src={imgSrc} alt="" />
                    ) : (
                      <>
                        <Webcam
                          audio={false}
                          ref={webcamRef}
                          screenshotFormat="image/jpeg"
                          forceScreenshotSourceSize="true"
                          videoConstraints={{
                            ...videoConstraints,
                            facingMode,
                            width: 500,
                            height: 260,
                          }}
                        />
                        {/* <div className="frame__camera frame1">1</div>
                        <div className="frame__camera frame2">2</div>
                        <div className="frame__camera frame3">3</div>
                        <div className="frame__camera frame4">4</div> */}
                        <div className="frame__camera">
                          <svg
                            width="100%"
                            height="100%"
                            viewBox="0 0 200 228"
                            fill="none"
                            xmlns="http://www.w3.org/2000/svg"
                          >
                            <path
                              d="M32.3486 4.17274H17.5001C10.5966 4.17274 5.00015 9.22615 5.00015 15.4599V28.8676"
                              stroke="white"
                              stroke-width="7"
                              stroke-linecap="round"
                            ></path>
                            <path
                              d="M5.00015 199.132V212.54C5.00015 218.773 10.5966 223.827 17.5001 223.827H32.3486"
                              stroke="white"
                              stroke-width="7"
                              stroke-linecap="round"
                            ></path>
                            <path
                              d="M195 28.8676V15.4599C195 9.22616 189.404 4.17273 182.5 4.17273H167.651"
                              stroke="white"
                              stroke-width="7"
                              stroke-linecap="round"
                            ></path>
                            <path
                              d="M167.651 223.827H182.5C189.404 223.827 195 218.773 195 212.54V199.132"
                              stroke="white"
                              stroke-width="7"
                              stroke-linecap="round"
                            ></path>
                          </svg>
                        </div>
                      </>
                    )}

                    {/* <div onClick={handleStopStream}>Stop Streaming</div>
                    <div onClick={handleClick}>Switch camera</div>
                    <div onClick={handleTakePhoto}>Take Photo</div>
                    {imgSrc && <img src={imgSrc} />} */}
                  </div>
                ) : (
                  <img src={paternImg} alt="" />
                )}
              </div>
              <div className="records">
                {!isStreaming && (
                  <img
                    onClick={() => {
                      // start();
                      handleStartStream();
                    }}
                    src={btnVoice}
                    alt=""
                  />
                )}

                {camera ? (
                  captureImg ? (
                    <>
                      <div onClick={handleTakePhotoBack} className="capture">
                        <HiArrowPath /> Chụp lại
                      </div>
                      {/* <div onClick={handleClick} className="capture">
                        Xoay
                      </div> */}
                    </>
                  ) : (
                    <>
                      <div onClick={handleTakePhoto} className="capture">
                        Chụp
                      </div>
                      <div onClick={handleClick} className="capture">
                        Xoay
                      </div>
                    </>
                  )
                ) : (
                  ""
                )}
              </div>
            </div>
            {captureImg && (
              <div className="continue">
                <div
                  onClick={() => {
                    setContinueImg2(true);
                  }}
                  className="btn__continue"
                >
                  Tiếp tục <BsFillArrowRightCircleFill />
                </div>
              </div>
            )}
            <div className="toast">
              <Toast />
            </div>
          </div>
          <div className="footer">
            <p>Power by</p>
            <img src={logovntelMini} alt="" />
          </div>
        </div>
      </Container>
    </>
  );
}
const wait = (time) => new Promise((resolve) => setTimeout(resolve, time));
const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  .frame__camera {
    position: absolute;
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
  }
  
  @media screen and (max-width: 500px) {
    .frame__camera {
      position: absolute;
      width: 100%;
      height: 75%;
      top: 0;
      left: 0;
      right: 0;
      bottom: 0;
      transform: translateY(16%);
    }
  }
  @media screen and (max-width: 400px) {
    .frame__camera {
      position: absolute;
      width: 100%;
      height: 60%;
      top: 0;
      left: 0;
      right: 0;
      bottom: 0;
      transform: translateY(34%);
    }
  }
  /* .frame1 {
    top: 40px;
    left: 10px;
  }
  .frame2 {
    top: 40px;
    right: 10px;
  }
  .frame3 {
    bottom: 40px;
    left: 10px;
  }
  .frame4 {
    bottom: 40px;
    right: 10px;
  } */

  .footer {
    margin: 20px 0;
    width: 100%;
    display: flex;
    align-items: center;
    gap: 10px;
    justify-content: center;
    p {
      margin-top: 10px;
      margin-bottom: 0;
    }
  }
  .content {
    display: flex;
    flex-direction: column;
    align-items: center;
    text-align: center;
    width: 100%;
    .spin__number {
      max-width: 1000px;
      width: 100%;
      height: 500px;
      border-radius: 8px;
      background: #fff;
      color: #000;
      display: flex;
      flex-direction: column;
      position: relative;
      #webcam-container {
        video {
          width: 100% !important;
          height: 100% !important;
        }
      }
      .continue {
        position: absolute;
        right: 20px;
        bottom: 40px;
      }
      .main__video {
        position: absolute;
        top: 100px;
        /* width: 100%; */
        left: 280px;
      }
      .spin__main {
        display: flex;
        flex-direction: column;
        align-items: center;

        .title {
          display: flex;
          flex-direction: column;
          align-items: center;
          h3 {
            color: #003e9c;
            font-weight: 500;
            font-size: 28px;
          }
        }
        .patern__img {
          img {
            height: 260px;
            object-fit: contain;
          }
        }
        .records {
          position: absolute;
          bottom: 20px;
          margin-top: 10px;
          cursor: pointer;
          display: flex;
          gap: 20px;
          align-items: center;
          .capture {
            padding: 10px 12px;
            background: #146c94;
            color: white;
            border-radius: 4px;
            div {
              display: flex;
              align-items: center;
            }
          }
          img {
            cursor: pointer;
            height: 70px;
          }
        }
      }

      .btn__continue {
        display: flex;
        gap: 10px;
        justify-content: flex-end;
        align-items: center;
        padding: 0 20px;
        color: #003e9c;
        cursor: pointer;
      }
    }
    h1 {
      font-size: 80px;
      line-height: 100px;
      font-family: "Kinn";
    }
    h3 {
      text-transform: uppercase;
      margin: 10px;
      font-weight: 400;
      letter-spacing: 2.2px;
    }
  }

  @media (max-width: 1365.98px) {
  }

  @media (max-width: 1199.98px) {
    .content .spin__number {
      max-width: 90%;
    }
  }

  @media (max-width: 991.98px) {
    .patern__img {
      > div {
        width: 100% !important;
      }
    }

    .spin__number .spin__main .records {
      gap: 10px !important;
    }
  }

  @media (max-width: 767.98px) {
    .logo__max {
      width: fit-content;
      margin: auto;
    }
  }

  @media (max-width: 575.98px) {
    .content {
      .title-big {
        font-size: 40px !important;
      }
    }
  }

  @media (max-width: 575.98px) {
    .continue {
      position: absolute;
      width: 60%;
      display: flex;
      justify-content: center;
      bottom: 10px !important;
    }
    .records {
      bottom: 30px !important;
    }
  }
  @media (max-width: 375.98px) {
    /* .spin__main {
      width: 288px !important;
    } */
  }
`;
const StyleBorder = styled.div`
  position: absolute;
  top: 96px;
  left: 276px;
  height: 240px;
  width: 460px;
  /* border-radius: 50%; */
  background: transparent;
  border: 4px solid green;
  z-index: 1000;
`;
const StyleBorderErr = styled.div`
  position: absolute;
  top: 96px;
  left: 276px;
  height: 240px;
  width: 460px;
  /* border-radius: 50%; */
  background: transparent;
  border: 4px solid red;
  z-index: 1000;
`;

export default SnapShotOneNew2;
