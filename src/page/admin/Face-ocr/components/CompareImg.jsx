import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import btnVoice from "../../../../assets/images/btn-take-photo.png";
import fileRecord from "../../../../assets/images/recordfile.png";
import { AiFillCamera, AiFillFileAdd } from "react-icons/ai";
import logovntelMini from "../../../../assets/images/logoMini.png";
import styled from "styled-components";
import { compareOcr, addPercentOcr , addDataOcr } from "../../../../features/VerifyOcr";
import LoadingDot from "../../../../components/loadingDot/LoadingDot";

function CompareImg({ setVerfify , setCompareTwoImg , setContinueImg2 }) {
  const dispatch = useDispatch();
  const img1 = useSelector((state) => state.verifyOcr.img1);
  const img2 = useSelector((state) => state.verifyOcr.img2);
  const [imageURL, setImageURL] = useState(null);
  const [renderImg1, setRenderImg1] = useState(img1);
  const [renderImg2, setRenderImg2] = useState(img2);
  const [loadding , setLoadding] = useState(false)
  const handVerify = async () => {
    // console.log('namdz >>>dsad')
    setLoadding(true)
    try {
      await dispatch(
        compareOcr({
          front: renderImg1,
          back: renderImg2,
        })
      )
        .unwrap()
        .then((res) => {
          dispatch(addPercentOcr(res.success));
          dispatch(addDataOcr(res.data));
          setLoadding(false)
          setVerfify(true);
        });
    } catch (error) {
      setVerfify(true);
      setLoadding(false)
    }
  };
  const handleFileInput1 = (event) => {
    const reader = new FileReader();
    reader.readAsDataURL(event.target.files[0]);
    reader.onload = () => {
      setRenderImg1(reader.result);
    };
  };
  const handleFileInput2 = (event) => {
    const reader = new FileReader();
    reader.readAsDataURL(event.target.files[0]);
    reader.onload = () => {
      setRenderImg2(reader.result);
    };
  };

  return (
    // <div>
    //   <img src={img1} alt="" />
    //   <img src={img2} alt="" />
    // </div>
    <Container>
      {loadding && <LoadingDot text='Chờ kết quả' /> }
      {imageURL && <img src={imageURL} alt="Uploaded image" />}
      <div className="content">
        <h1 className="title-big">OCR</h1>
        <h3>So sánh OCR</h3>
        <div className="spin__number">
          <div className="spin__main">
            <div className="main__content">
              <div className="title__header">
                <div>File hình ảnh 1</div>
                <div>File hình ảnh 2</div>
              </div>
              <div className="main__section">
                <div className="item1">
                  <div className="item1__section">
                    <img src={renderImg1} alt="" />
                    <div className="record__back">
                      <div onClick={()=>setContinueImg2(false)}>
                        <AiFillCamera /> Chụp lại
                      </div>
                    </div>
                  </div>
                  <div className="upload__file">
                    {/* <AiFillFileAdd /> upload file  */}
                    <input type="file" onChange={handleFileInput1} />
                  </div>
                </div>
                <div className="item1">
                  <div className="item1__section">
                    <img src={renderImg2} alt="" />
                    <div className="record__back">
                      <div onClick={()=>setCompareTwoImg(false)}>
                        <AiFillCamera /> Chụp lại
                      </div>
                    </div>
                  </div>
                  <div className="upload__file">
                    {/* <AiFillFileAdd /> upload file */}
                    <input type="file" onChange={handleFileInput2} />
                  </div>
                </div>
              </div>
            </div>
            <div className="records">
              <div onClick={() => handVerify()}>Xác thực</div>
            </div>
          </div>
        </div>
        <div className="footer">
          <p>Power by</p>
          <img src={logovntelMini} alt="" />
        </div>
      </div>
    </Container>
  );
}
const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  .footer {
    margin: 20px 0;
    width: 100%;
    display: flex;
    align-items: center;
    gap: 10px;
    justify-content: center;
    p {
      margin-top: 10px;
      margin-bottom: 0;
    }
  }
  .content {
    display: flex;
    flex-direction: column;
    align-items: center;
    width: 100%;
    .spin__number {
      max-width: 1000px;
      width: 100%;
      height: 460px;
      border-radius: 8px;
      background: #fff;
      color: #000;
      display: flex;
      flex-direction: column;
      .spin__main {
        display: flex;
        flex-direction: column;
        align-items: center;
        .main__content {
          margin-top: 20px;
          .title__header {
            border: 1px solid black;
            display: flex;
            height: 60px;
            div {
              flex: 1;
              height: 100%;
              display: flex;
              align-items: center;
              justify-content: center;
              &:nth-child(1) {
                border-right: 1px solid black;
              }
            }
          }
          .main__section {
            border: 1px solid black;
            border-top: 0;
            display: flex;
            height: 300px;
            .item1 {
              flex: 1;
              display: flex;
              justify-content: center;
              padding: 10px;
              display: flex;
              flex-direction: column;
              .item1__section {
                display: flex;
                flex-direction: column;
                align-items: center;
                gap: 12px;
                img {
                  height: 140px;
                  object-fit: contain;
                }
                .record__back {
                  cursor: pointer;
                  div {
                    cursor: pointer;
                    width: 100px;
                    background: #0065ff;
                    padding: 10px 12px;
                    gap: 4px;
                    display: flex;
                    justify-content: center;
                    align-items: center;
                    border-radius: 100px;
                    font-size: 16px;
                    font-weight: bold;
                    color: #fff;
                    box-sizing: unset;
                  }
                }
              }
              .upload__file {
                cursor: pointer;
                display: flex;
                justify-content: flex-end;
                gap: 4px;
                text-decoration: underline;
                color: #0065ff;
              }
              &:nth-child(1) {
                border-right: 1px solid black;
              }
            }
          }
          display: flex;
          flex-direction: column;
          /* background: #e6f0ff; */
          height: 340px;
          max-width: 800px;
          width: 100%;
        }
        .records {
          margin-top: 40px;
          div {
            width: 80px;
            padding: 12px 18px;
            border-radius: 100px;
            display: flex;
            justify-content: center;
            align-items: center;
            font-weight: 550;
            color: #fff;
            cursor: pointer;
            box-sizing: unset;
            background: linear-gradient(
              143.87deg,
              #2b7fff 19.23%,
              #0065ff 32.96%,
              #0047b3 59.19%,
              #003e9c 81.67%
            );
          }
        }
      }

      .btn__continue {
        display: flex;
        gap: 10px;
        justify-content: flex-end;
        align-items: center;
        padding: 0 20px;
        color: #003e9c;
        cursor: pointer;
      }
    }
    h1 {
      font-size: 80px;
      line-height: 100px;
      font-family: "Kinn";
    }
    h3 {
      text-transform: uppercase;
      margin: 10px;
      font-weight: 400;
      letter-spacing: 2.2px;
      text-align: center;
    }
  }
  .upload__file {
    margin-top: 10px;
  }

  @media (max-width: 1365.98px) {
  }

  @media (max-width: 1199.98px) {
    .content .spin__number {
      max-width: 90%;
    }
  }

  @media (max-width: 991.98px) {
    .patern__img {
      > div {
        width: 100% !important;
      }
    }

    .spin__number .spin__main .records {
      gap: 10px !important;
    }
  }

  @media (max-width: 767.98px) {
    .logo__max {
      width: fit-content;
      margin: auto;
    }
  }

  @media (max-width: 575.98px) {
    .content {
      .title-big {
        font-size: 40px !important;
      }
      .records {
        margin-top: 0px !important;
        margin-bottom: 20px !important;
      }
    }

    .content .spin__number {
      height: auto !important;
    }

    .content .spin__number .spin__main .main__content .title__header {
      display: none !important;
    }

    .content .spin__number .spin__main .main__content .main__section {
      display: block !important;
      height: auto;
    }

    .spin__number .spin__main .main__content .main__section .item1 {
      margin-bottom: 20px;
    }
    .main__content {
      height: auto !important;
      .main__section {
        border: none !important;
      }
    }
  }

  @media (max-width: 375.98px) {
  }
`;

export default CompareImg;
