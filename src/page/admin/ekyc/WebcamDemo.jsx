import React, { useCallback, useEffect, useRef, useState } from "react";

import Webcam from "react-webcam";
import FaceDetection from "@mediapipe/face_detection";
import { Camera } from "@mediapipe/camera_utils";

import { contains, Rect } from "./contains";
import CustomProcessBar from "./CustomProcessBar";
import useFaceDetection from "./hooks/useFaceDetection";
const width = 500;
const height = 500;

const PREVIEW_SIZE = 290;
const PREVIEW_RECT = {
  minX: (window.innerWidth - PREVIEW_SIZE) / 2,
  minY: 50,
  width: PREVIEW_SIZE,
  height: PREVIEW_SIZE,
};

const styles = {
  container: {
    // display: "flex",
    // justifyContent: "center",
    // position: "relative",
  },
  mask: {
    borderRadius: "50%",
    height: PREVIEW_SIZE,
    width: PREVIEW_SIZE,
    // marginTop: PREVIEW_RECT.minY,
    alignSelf: "center",
    backgroundColor: "white",
    position: "relative",
    overflow: "hidden",
  },
  circularProgress: {
    width: PREVIEW_SIZE,
    height: PREVIEW_SIZE,
    marginTop: PREVIEW_RECT.minY,
    marginLeft: PREVIEW_RECT.minX,
  },
  instructions: {
    fontSize: 20,
    textAlign: "center",
    top: 25,
    position: "absolute",
  },
  instructionsContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: PREVIEW_RECT.minY + PREVIEW_SIZE,
  },
  action: {
    fontSize: 24,
    textAlign: "center",
    fontWeight: "bold",
  },
};
const WebcamDemo = ({ setImgSrc, setCaptureImg }) => {
  const [status, setStatus] = useState("");
  const [img, setImg] = useState(null);
  const [counter, setCounter] = React.useState(0);

  // Progress from 0 to 100
  var progress = counter > 0 ? counter / 5 : 0;

  // Status from "Stopped" to "Done"
  var percentage = 100 * progress;

  const { webcamRef, boundingBox, isLoading, detected, facesDetected } =
    useFaceDetection({
      faceDetectionOptions: {
        model: "short",
      },
      faceDetection: new FaceDetection.FaceDetection({
        locateFile: (file) =>
          `https://cdn.jsdelivr.net/npm/@mediapipe/face_detection/${file}`,
      }),
      camera: ({ mediaSrc, onFrame }) =>
        new Camera(mediaSrc, {
          onFrame,
          width,
          height,
        }),
    });
 
  useEffect(() => {
    if (detected === false) {
      setStatus({ status: "invalid", text: "Ko tìm thấy gương mặt nào" });
      return;
    }
    if (detected === true && facesDetected > 1) {
      setStatus({
        status: "invalid",
        text: "Quá nhiều gương mặt trong khung hình",
      });
      return;
    }
    if (detected === true && boundingBox.length > 0) {
      const faceRect = {
        minX: boundingBox[0].xCenter,
        minY: boundingBox[0].yCenter,
        width: boundingBox[0].width * 100 + 100,
        height: boundingBox[0].height * 100 + 100,
      };

      // 2. The face is almost fully contained within the camera preview.
      const edgeOffset = 50;
      const faceRectSmaller = {
        width: faceRect.width - edgeOffset,
        height: faceRect.height - edgeOffset,
        minY: faceRect.minY + edgeOffset / 2,
        minX: faceRect.minX + edgeOffset / 2,
      };
      const previewContainsFace = contains({
        outside: PREVIEW_RECT,
        inside: faceRectSmaller,
      });

      const faceMaxSize = PREVIEW_SIZE - 100;

      console.log(faceRect.width);
      console.log(faceRect.height);
      console.log(faceMaxSize);

      console.log(boundingBox);

      if (faceRect.width > faceMaxSize && faceRect.height > faceMaxSize) {
        console.log("1");
        setStatus({
          status: "invalid",
          text: "Khuôn mặt không nằm trọn vẹn trong khung ảnh",
        });

        return;
      }
      if (faceRect.width < 133 && faceRect.height < 133) {
        console.log("1");
        setStatus({
          status: "invalid",
          text: "Di chuyển lại gần Camera",
        });

        return;
      }
      if (faceRect.minY < 0.3) {
        setStatus({
          status: "invalid",
          text: "Khuôn mặt không nằm trọn vẹn trong khung ảnh",
        });
        console.log("1");
        return;
      }
      if (faceRect.minY > 0.45) {
        setStatus({
          status: "invalid",
          text: "Khuôn mặt không nằm trọn vẹn trong khung ảnh",
        });
        console.log("1");
        return;
      }
      if (Math.sign(faceRect.minX) === -1) {
        setStatus({
          status: "invalid",
          text: "Khuôn mặt không nằm trọn vẹn trong khung ảnh",
        });
        console.log("1");
        return;
      }
      if (faceRect.minX > 0.45) {
        // right
        setStatus({
          status: "invalid",
          text: "Khuôn mặt không nằm trọn vẹn trong khung ảnh",
        });
        console.log("1");
        return;
      }
      if (faceRect.minX < 0.15) {
        setStatus({
          status: "invalid",
          text: "Khuôn mặt không nằm trọn vẹn trong khung ảnh",
        });
        console.log("1");
        return;
      }
      return setStatus({ status: "valid", text: "" });
    }
  }, [facesDetected, detected, boundingBox]);

  const capture = useCallback(() => {
    const imageSrc = webcamRef.current.getScreenshot();
    setImg(imageSrc);
  }, [webcamRef]);

  // React.useEffect(() => {
  //   if (status?.status === "valid") {

  //     const timer = setTimeout(capture, 3000);
  //     return () => clearTimeout(timer);
  //   }
  // }, [status?.status]);
  React.useEffect(() => {
    console.log(img);
    if (img) {
      setImgSrc(img);
      setCaptureImg(true);
    }
  }, [img]);

  const checkStatus = (status) => {
    if (status === "invalid") {
      return "invalid";
    }
    if (status === "capture") {
      return "capture";
    }
    return "";
  };

  const delay = 1;
  const timer = useRef(null); // we can save timer in useRef and pass it to child

  React.useEffect(() => {
    if (status?.status === "valid" && counter < 5) {
      // useRef value stored in .current property

      timer.current = setInterval(() => setCounter((v) => v + 1), delay * 1000);

      // clear on component unmount
      return () => {
        clearInterval(timer.current);
      };
    }
    if (status?.status === "invalid") {
      setCounter(0);
    }
  }, [status.status, counter]);

  useEffect(() => {
    if (counter === 5 && status?.status === "valid") {
      capture();
    }
    if (counter < 5) return;
    //eslint-disable-next-line
  }, [counter, status.status]);

  return (
    <div className="realtime-ekyc">
      {/* <p>{`trang thai: ${status?.text}`}</p>
      <p>{`Loading: ${isLoading}`}</p>
      <p>{`Face Detected: ${detected}`}</p>
      <p>{counter}</p>
      <p>{`Number of faces detected: ${facesDetected}`}</p> */}
      {img === null ? (
        <div style={styles.container}>
          <div style={styles.mask}>
            {boundingBox.map((box, index) => (
              <div key={`${index + 1}`} />
            ))}
            <div className={`video-container ${checkStatus(status?.status)}`}>
              {status?.status === "invalid" && (
                <div className="message-invalid">{status?.text}</div>
              )}
              <div className="ekyc-frame">
                <svg
                  width="100%"
                  height="100%"
                  viewBox="0 0 200 228"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    d="M32.3486 4.17274H17.5001C10.5966 4.17274 5.00015 9.22615 5.00015 15.4599V28.8676"
                    stroke="white"
                    stroke-width="7"
                    stroke-linecap="round"
                  ></path>
                  <path
                    d="M5.00015 199.132V212.54C5.00015 218.773 10.5966 223.827 17.5001 223.827H32.3486"
                    stroke="white"
                    stroke-width="7"
                    stroke-linecap="round"
                  ></path>
                  <path
                    d="M195 28.8676V15.4599C195 9.22616 189.404 4.17273 182.5 4.17273H167.651"
                    stroke="white"
                    stroke-width="7"
                    stroke-linecap="round"
                  ></path>
                  <path
                    d="M167.651 223.827H182.5C189.404 223.827 195 218.773 195 212.54V199.132"
                    stroke="white"
                    stroke-width="7"
                    stroke-linecap="round"
                  ></path>
                </svg>
              </div>
              <CustomProcessBar value={percentage} strokeWidth={2}>
                <Webcam
                  audio={false}
                  imageSmoothing={true}
                  ref={webcamRef}
                  screenshotQuality={1}
                  screenshotFormat="image/jpeg"
                  forceScreenshotSourceSize={true}
                  style={{
                    height: PREVIEW_SIZE,
                    width: PREVIEW_SIZE,
                    borderRadius: "50%",
                    position: "absolute",
                    zIndex: 10,
                  }}
                />
              </CustomProcessBar>
            </div>
          </div>
        </div>
      ) : (
        <>
          <p>Xem lai anh chup</p>
          <img src={img} alt="screenshot" />
        </>
      )}
    </div>
  );
};

export default WebcamDemo;
