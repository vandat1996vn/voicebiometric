import React, { useState, useEffect } from "react";
import styled from "styled-components";
import btnBack from "../../../../assets/images/btnback.png";
import btnVoice from "../../../../assets/images/btnVoice.png";
import btnVoiceSecond from "../../../../assets/images/button-voice-2.png";
import { BsFillArrowRightCircleFill } from "react-icons/bs";
import { IoMdSave } from "react-icons/io";
import { CountdownCircleTimer } from "react-countdown-circle-timer";
import Recorder from "recorder-js";
import { Link } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import {
  voiceRecordTwo,
  selectVoiceTwo
} from "../../../../features/VerifyVoice";
import SpeechRecognition, {
  useSpeechRecognition
} from "react-speech-recognition";

function EnterNumberSecond({ setRecord, setVerify }) {
  const dispatch = useDispatch();
  const voiceTwo = useSelector(selectVoiceTwo);
  const [characters, setCharacters] = useState();
  const [startVoice, setStartVoice] = useState(false);
  const [mediaStream, setMediaStream] = useState(null);
  const [recorder, setRecorder] = useState(null);
  const [isRecording, setIsRecording] = useState(false);
  const [audioBlob, setAudioBlob] = useState(null);
  const [audioSrc, setAudioSrc] = useState(null);
  const [countdown, setCountdown] = useState(10);
  const [nextState, setNextState] = useState(false);
  const [checkVoice, setCheckVoice] = useState();
  const [transVoice, setTransVoice] = useState();
  const [activeSave, setActiveSave] = useState(false);

  console.log("voice One", voiceTwo);

  const {
    transcript,
    listening,
    browserSupportsSpeechRecognition,
    resetTranscript
  } = useSpeechRecognition({ autoStart: false , language: 'vi-VN' });

  const startListening = () =>
    SpeechRecognition.startListening({ continuous: true });

  const stopListening = () =>
    SpeechRecognition.stopListening({ continuous: true });

  let stringTest = [
    { number: "0", value: "Không" },
    { number: "1", value: "Một" },
    { number: "2", value: "Hai" },
    { number: "3", value: "Ba" },
    { number: "4", value: "Bốn" },
    { number: "5", value: "Năm" },
    { number: "6", value: "Sáu" },
    { number: "7", value: "Bảy" },
    { number: "8", value: "Tám" },
    { number: "9", value: "Chín" }
  ];

  const shuffleArray = (arr) => {
    for (let i = arr.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [arr[i], arr[j]] = [arr[j], arr[i]];
    }
    return arr;
  };

  const handleStartRecording = () => {
    recorder.start().then(() => {
      resetTranscript();
      setIsRecording(true);
      startListening();
      setTransVoice(null);
      setActiveSave(false);
      setAudioBlob(null);
    });
  };

  const handleStopRecording = () => {
    recorder.stop().then(({ blob, buffer }) => {
      // stopListening();
      setAudioBlob(blob);
      setIsRecording(false);
      setAudioSrc(URL.createObjectURL(blob));
      // if (mediaStream) {
      //   mediaStream.getAudioTracks()[0].stop();
      // }
      // if (checkVoice === transVoice) {
      //   setActiveSave(true);
      // } else {
      //   alert("phát âm chưa đúng , hãy thử lại !");
      // }
    });
  };

  const handleDownload = () => {
    Recorder.download(audioBlob, "my-audio-file"); // downloads a .wav file
  };

  const handleGetUserMedia = () => {
    navigator.mediaDevices
      .getUserMedia({ audio: true })
      .then((stream) => {
        const audioContext = new (window.AudioContext ||
          window.webkitAudioContext)();
        const newRecorder = new Recorder(audioContext, {
          // onAnalysed: (data) => console.log(data),
        });
        newRecorder.init(stream);
        setRecorder(newRecorder);
        setMediaStream(stream);
      })
      .catch((error) => console.error(error));
  };

  useEffect(() => {
    if (stringTest) {
      setCharacters(shuffleArray(stringTest));
    }
    handleGetUserMedia();
  }, []);

  useEffect(() => {
    setCheckVoice(
      characters
        ?.map((item) => {
          return item.number;
        })
        .join("")
    );
  }, [characters]);

  useEffect(() => {
    if (transcript) {
      setTransVoice(transcript.replace(/\s/g, ""));
      // SpeechRecognition.stopListening()
    }
  }, [transcript]);

  console.log("checkvoice", checkVoice);
  // console.log("transvoice", transVoice);

  const handleActiveVoice = () => {
    setStartVoice(!startVoice);
    setCountdown(10);
    setNextState(false);
  };

  useEffect(() => {
    if (startVoice === true) {
      const intervalId = setInterval(() => {
        setCountdown((prevCountdown) => prevCountdown - 1);
      }, 1000);
      if (countdown === 0) {
        setStartVoice(false);
        handleStopRecording();
        clearInterval(intervalId);
      }
      return () => {
        clearInterval(intervalId);
      };
    }
  }, [countdown, startVoice]);

  const handleReset = () => {
    setCharacters(shuffleArray(stringTest));
    setCountdown(10);
    setStartVoice(false);
    setAudioSrc(null);
    setAudioBlob(null);
    setNextState(false);
    stopListening();
    resetTranscript();
    setActiveSave(false);
  };

  const handleSaveRecorde = () => {
    dispatch(voiceRecordTwo(audioBlob));
    setAudioSrc(null);
    setNextState(true);
  };

  console.log("audio", audioBlob);

  return (
    <Container>
      <div className="content">
        <h1
          className="title-big"
          onClick={() => {
            setRecord(true);
            setVerify(false);
          }}
        >
          Voice Biometrics
        </h1>
        <h3>Giải pháp sinh trắc học giọng nói</h3>
        <div className="spin__number">
        <Link
          style={{
            color: "#1653af",
            display: "flex",
            width: "fit-content",
            marginLeft: "auto"
          }}
          to="/"
        >
          Về trang chủ
        </Link>
          <div className="spin__main">
            <div className="title" style={{ marginBottom: 40 }}>
              <h3>Ghi âm giọng nói lần 2 </h3>
              <p>Nhấn nút ghi và đọc lại dãy số sau</p>
            </div>
            <div className="main__content">
              <div className="spin__back" onClick={() => handleReset()}>
                <img src={btnBack} alt="" />
              </div>
              <ul className="list-voice__number">
                {characters?.map((item, index) => (
                  <li key={index}>
                    <p
                      style={{
                        fontSize: 48,
                        fontWeight: 500,
                        color: "#003E9C",
                        marginBottom: 15
                      }}
                    >
                      {item.number}
                    </p>
                    <p style={{ fontSize: 14, color: "#909DAD" }}>
                      {item.value}
                    </p>
                  </li>
                ))}
              </ul>
            </div>
            {/* {transVoice && (
              <p style={{ color: "#1C76FF", marginTop: 20 }}>
                {" "}
                Phát âm : {transVoice}
              </p>
            )} */}
            <div className="records" onClick={() => handleActiveVoice()}>
              {startVoice === true ? (
                <>
                  <CountdownCircleTimer
                    isPlaying
                    trailColor={["white"]}
                    duration={10}
                    size={180}
                    // rotation={["counterclockwise"]}
                    colors={["red"]}
                    strokeWidth={2}
                    onComplete={() => ({ shouldRepeat: false, delay: 0 })}
                  ></CountdownCircleTimer>
                  <img
                    src={btnVoiceSecond}
                    alt=""
                    onClick={() => handleStopRecording()}
                  />
                </>
              ) : (
                <img
                  src={btnVoice}
                  alt=""
                  onClick={() => handleStartRecording()}
                />
              )}
            </div>
            {audioSrc && <audio src={audioSrc} controls style={{marginTop:10}} />}
          </div>
          {audioSrc && (
            <div
              onClick={() => handleSaveRecorde()}
              className="next-photo__second"
            >
              Lưu <IoMdSave />
            </div>
          )}
          {nextState === true && (
            <div
              onClick={() => {
                setRecord(true);
                setVerify(false);
              }}
              className="btn__continue"
            >
              Tiếp tục <BsFillArrowRightCircleFill />
            </div>
          )}
        </div>
      </div>
    </Container>
  );
}
const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  .list-voice__number {
    display: flex;
    flex-wrap: wrap;
    justify-content: center;
    text-align: center;

    li:not(:last-child) {
      margin-right: 30px;
    }
  }
  .content {
    display: flex;
    flex-direction: column;
    align-items: center;
    margin-bottom: 100px;
    text-align: center;
    width: 100%;
    .spin__number {
      padding: 20px;
      width: 100%;
      max-width: 1000px;
      border-radius: 8px;
      background: #fff;
      color: #000;
      display: flex;
      flex-direction: column;
      .spin__main {
        display: flex;
        flex-direction: column;
        align-items: center;
        .title {
          display: flex;
          flex-direction: column;
          align-items: center;
          h3 {
            color: #003e9c;
            font-weight: 500;
            font-size: 28px;
          }
        }
        .main__content {
          padding: 10px;
          display: flex;
          flex-direction: column;
          background: #e6f0ff;
          border-radius: 20px;
          .spin__back {
            display: flex;
            justify-content: flex-end;
            margin-bottom: 10px;
            cursor: pointer;
          }
          .flex {
            div {
              font-size: 45px;
              font-weight: bold;
              color: #003e9c;
              display: flex;
              justify-content: center;
              flex: 1;
            }
          }
          .text {
            margin-top: 10px;
            div {
              font-size: 14px;
              color: #000;
              font-weight: 500;
              text-transform: uppercase;
            }
          }
        }
        .records {
          cursor: pointer;
          margin-top: 10px;
          width: fit-content;
          height: fit-content;
          display: flex;
          align-items: center;
          justify-content: center;
          position: relative;
          width: 180px;
          height: 180px;

          img {
            position: absolute;
            max-width: 177px;
            max-height: 177px;
            border-radius: 1000px;
            object-fit: scale-down;
          }
        }
      }

      .btn__continue {
        display: flex;
        gap: 10px;
        justify-content: flex-end;
        align-items: center;
        padding: 0 20px;
        color: #003e9c;
        cursor: pointer;
      }
    }
    h1 {
      font-size: 80px;
      margin-bottom: 40px;
      font-family: "Kinn";
    }
    h3 {
      text-transform: uppercase;
      margin: 10px;
      font-weight: 400;
      letter-spacing: 2.2px;
    }
  }

  @media (max-width: 1365.98px) {
  }

  @media (max-width: 1199.98px) {
    .content .spin__number {
      max-width: 90%;
    }
  }

  @media (max-width: 991.98px) {
  }

  @media (max-width: 767.98px) {
    .logo__max {
      width: fit-content;
      margin: auto;
    }

    .list-voice__number {
      li {
        p:nth-child(1) {
          font-size: 25px !important;
        }
      }
    }
    .content .spin__number .spin__main .records {
      width: 80px;
      height: 80px;

      > div {
        width: 80px !important;
        height: 80px !important;

        svg {
          width: 80px !important;
          height: 80px !important;
        }
      }

      img {
        max-width: 78px;
        max-height: 78px;
      }
    }
  }

  @media (max-width: 575.98px) {
    .content {
      .title-big {
        font-size: 40px !important;
      }
    }
  }

  @media (max-width: 375.98px) {
  }
`;

export default EnterNumberSecond;
