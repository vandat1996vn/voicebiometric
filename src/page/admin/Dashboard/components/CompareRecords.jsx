import React, { useState, useEffect } from "react";
import styled from "styled-components";
import fileRecord from "../../../../assets/images/recordfile.png";
import { AiFillAudio, AiFillFileAdd } from "react-icons/ai";
import { useSelector, useDispatch } from "react-redux";
import {
  selectVoiceOne,
  selectVoiceTwo,
  compareVoice,
  voiceResultAction,
} from "../../../../features/VerifyVoice";
import LoadingDot from "../../../../components/loadingDot/LoadingDot";

function CompareRecords({ setVerify, setRecord, setRecordSecond }) {
  const dispatch = useDispatch();
  const [voiceOne, setVoiceOne] = useState(useSelector(selectVoiceOne));
  const [voiceTwo, setVoiceTwo] = useState(useSelector(selectVoiceTwo));
  const [loadding, setLoadding] = useState(false);

  // const [audioFileOne, setAudioFileOne] = useState(null);
  const [fileNameOne, setFileNameOne] = useState("");
  // const [audioFileTwo, setAudioFileTwo] = useState(null);
  const [fileNameTwo, setFileNameTwo] = useState("");

  const handleFileChange = (event, number) => {
    const file = event.target.files[0];
    const reader = new FileReader();

    reader.onload = (event) => {
      const blob = new Blob([event.target.result], { type: file.type });
      if (number === "one") {
        setVoiceOne(blob);
        setFileNameOne(file.name);
      }

      if (number === "two") {
        setVoiceTwo(blob);
        setFileNameTwo(file.name);
      }
    };

    reader.readAsArrayBuffer(file);
  };

  // const audioBlob1 = new File([voiceOne], "my_voice1.png",{type:"audio/wav", lastModified:new Date().getTime()})
  // const audioBlob2 = new Blob([voiceTwo], { type: 'audio/wav' });

  // console.log("UpOne", audioFileOne);
  // console.log("UpTwo", audioFileTwo);

  console.log("one", voiceOne);
  console.log("two", voiceTwo);

  // useEffect(() => {}, []);

  const voice = new FormData();
  voiceOne && voice.append("utterance_target", voiceOne);
  voiceTwo && voice.append("utterance_source", voiceTwo);

  // console.log("formData", voice);

  const handleSuscess = () => {
    console.log(voiceOne);
    console.log(voiceTwo);
    setLoadding(true);
    dispatch(compareVoice(voice))
      .unwrap()
      .then((res) => {
        console.log("res", res);
        dispatch(voiceResultAction(res));
        setLoadding(false);
        setVerify(true);
      })
      .catch((error) => {
        console.log(error);
        setLoadding(false);
      });
  };

  return (
    <Container>
      {loadding && <LoadingDot text="Chờ kết quả" />}
      <div className="content">
        <h1
          className="title-big"
          style={{ fontSize: 106, marginBottom: 10, fontWeight: 600 }}
        >
          Voice Biometrics
        </h1>
        <h3>Giải pháp sinh trắc học giọng nói</h3>
        <div className="spin__number">
          <div className="spin__main">
            <div className="main__content">
              <div className="title__header">
                <div>File ghi âm 1</div>
                <div>File ghi âm 2</div>
              </div>
              <div className="main__section">
                <div className="item1">
                  <div className="item1__section">
                    <img src={fileRecord} alt="" />
                    <audio
                      style={{ width: "100%" }}
                      src={voiceOne && URL.createObjectURL(voiceOne)}
                      controls
                    />
                    <div className="record__back">
                      <div
                        onClick={() => {
                          setRecord(false);
                          setRecordSecond(false);
                        }}
                        style={{ cursor: "pointer", marginBottom: 20 }}
                      >
                        <AiFillAudio /> Ghi lại
                      </div>
                    </div>
                  </div>
                  <div className="upload__file">
                    <div className="upload-form__all">
                      <input
                        type="file"
                        accept="audio/*"
                        onChange={(e) => handleFileChange(e, "one")}
                      />
                      <label>
                        <AiFillFileAdd />
                        {fileNameOne ? fileNameOne : "upload file"}
                      </label>
                    </div>
                  </div>
                </div>
                <div className="item1">
                  <div className="item1__section">
                    <img src={fileRecord} alt="" />
                    <audio
                      style={{ width: "100%" }}
                      src={voiceOne && URL.createObjectURL(voiceTwo)}
                      controls
                    />
                    <div className="record__back">
                      <div
                        onClick={() => {
                          setRecord(false);
                          setRecordSecond(true);
                        }}
                        style={{ cursor: "pointer", marginBottom: 20 }}
                      >
                        <AiFillAudio /> Ghi lại
                      </div>
                    </div>
                  </div>
                  <div className="upload__file">
                    <div className="upload-form__all">
                      <input
                        type="file"
                        accept="audio/*"
                        onChange={(e) => handleFileChange(e, "two")}
                      />
                      <label>
                        <AiFillFileAdd />
                        {fileNameTwo ? fileNameTwo : "upload file"}
                      </label>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="records" onClick={() => handleSuscess()}>
              <div>Xác thực</div>
            </div>
          </div>
        </div>
      </div>
    </Container>
  );
}
const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  .content {
    display: flex;
    flex-direction: column;
    align-items: center;
    margin-bottom: 100px;
    padding: 20px;
    .spin__number {
      max-width: 1000px;
      width: 100%;
      border-radius: 8px;
      background: #fff;
      padding: 20px;
      color: #000;
      display: flex;
      flex-direction: column;
      .spin__main {
        display: flex;
        flex-direction: column;
        align-items: center;
        .main__content {
          margin-top: 20px;
          .title__header {
            border: 1px solid black;
            display: flex;
            div {
              flex: 1;
              display: flex;
              align-items: center;
              justify-content: center;
              padding: 10px;
              &:nth-child(1) {
                border-right: 1px solid black;
              }
            }
          }
          .main__section {
            border: 1px solid black;
            border-top: 0;
            display: flex;
            .item1 {
              flex: 1;
              display: flex;
              justify-content: center;
              padding: 10px;
              display: flex;
              flex-direction: column;
              .item1__section {
                display: flex;
                flex-direction: column;
                align-items: center;
                gap: 12px;
                img {
                  height: 50px;
                }
                .record__back {
                  div {
                    width: 60px;
                    background: #0065ff;
                    padding: 10px 12px;
                    gap: 4px;
                    display: flex;
                    justify-content: center;
                    align-items: center;
                    border-radius: 100px;
                    font-size: 12px;
                    font-weight: bold;
                    color: #fff;
                  }
                }
              }
              .upload__file {
                display: flex;
                justify-content: flex-end;
                gap: 4px;
                text-decoration: underline;
                color: #0065ff;
              }
              &:nth-child(1) {
                border-right: 1px solid black;
              }
            }
          }
          display: flex;
          flex-direction: column;
          background: #e6f0ff;
          max-width: 600px;
          width: 100%;
        }
        .records {
          margin-top: 40px;
          div {
            /* width: 80px; */
            padding: 12px 18px;
            border-radius: 100px;
            display: flex;
            justify-content: center;
            align-items: center;
            font-weight: 550;
            color: #fff;
            cursor: pointer;
            background: linear-gradient(
              143.87deg,
              #2b7fff 19.23%,
              #0065ff 32.96%,
              #0047b3 59.19%,
              #003e9c 81.67%
            );
          }
        }
        .upload-form__all {
          position: relative;
          width: fit-content;
          margin-left: auto;
          margin-right: auto;

          input {
            position: absolute;
            left: 0;
            top: 0;
            width: 100%;
            height: 100%;
            z-index: 10;
            opacity: 0;
          }

          svg {
            margin-right: 10px;
          }

          label {
            display: -webkit-box;
            -webkit-box-orient: vertical;
            -webkit-line-clamp: 1;
            overflow: hidden;
          }
        }
      }

      .btn__continue {
        display: flex;
        gap: 10px;
        justify-content: flex-end;
        align-items: center;
        padding: 0 20px;
        color: #003e9c;
        cursor: pointer;
      }
    }
    h1 {
      font-size: 80px;
      font-family: "Kinn";
      margin-bottom: 40px;
    }
    h3 {
      text-transform: uppercase;
      margin: 10px;
      font-weight: 400;
      letter-spacing: 2.2px;
    }
  }

  @media (max-width: 1365.98px) {
  }

  @media (max-width: 1199.98px) {
    .content .spin__number {
      max-width: 90%;
    }
  }

  @media (max-width: 991.98px) {
  }

  @media (max-width: 767.98px) {
    .logo__max {
      width: fit-content;
      margin: auto;
    }

    .list-voice__number {
      li {
        p:nth-child(1) {
          font-size: 25px !important;
        }
      }
    }

    .main__section {
      border-top: 1px solid !important;
    }

    .content .spin__number .spin__main .main__content .title__header {
      display: none;
    }

    .content .spin__number .spin__main .main__content .main__section {
      flex-wrap: wrap;
    }

    .content .spin__number .spin__main .main__content .main__section .item1 {
      margin-bottom: 30px;
      width: 100%;
      flex: unset !important;
    }
  }

  @media (max-width: 575.98px) {
    .content {
      .title-big {
        font-size: 40px !important;
      }
    }
  }

  @media (max-width: 375.98px) {
  }
`;

export default CompareRecords;
