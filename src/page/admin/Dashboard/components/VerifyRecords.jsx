import React, { useEffect, useState } from "react";
import styled from "styled-components";
import resultRecords from "../../../../assets/images/result1.png";
import { AiOutlineRedo } from "react-icons/ai";
import {
  CircularProgressbar,
  CircularProgressbarWithChildren,
  buildStyles
} from "react-circular-progressbar";
import { useSelector, useDispatch } from "react-redux";
import RadialSeparators from "./RadialSeparators";
import ChangingProgressProvider from "./ChangingProgressProvider";
import {
  selectVoiceResult,
  voiceRecordOne,
  voiceRecordTwo
} from "../../../../features/VerifyVoice";

function VerifyRecords({ setRecord, setRecordSecond, setEnterNumber }) {
  const voiceResult = useSelector(selectVoiceResult);
  const dispatch = useDispatch();

  const handleRefreshAction = () => {
    dispatch(voiceRecordOne());
    dispatch(voiceRecordTwo());
  };

  return (
    <Container>
      <div className="content">
        <h1 className="title-big">Voice Biometrics</h1>
        <h3>Giải pháp sinh trắc học giọng nói</h3>
        <div className="spin__number">
          <div className="spin__main">
            <div className="title">
              <h3>Kết quả xác thực</h3>
            </div>
            <div className="result">
              <ChangingProgressProvider
                values={[0, voiceResult?.score?.toFixed(2) * 100]}
              >
                {(value) => (
                  <CircularProgressbarWithChildren
                    value={value}
                    text={`${value}%`}
                    strokeWidth={10}
                    styles={buildStyles({
                      rotation: 0,
                      strokeLinecap: "butt",
                      trailColor: "#eee"
                    })}
                  >
                    <RadialSeparators
                      count={12}
                      style={{
                        background: "#fff",
                        width: "2px",
                        height: `${10}%`
                      }}
                    />
                  </CircularProgressbarWithChildren>
                )}
              </ChangingProgressProvider>
            </div>
            <div className="result__text">
              <p>
                Giọng nói trùng khớp {`${voiceResult?.score?.toFixed(2) * 100}`}
                %
              </p>
            </div>
          </div>
          <div className="roll__back">
            <p
              onClick={() => {
                handleRefreshAction();
                setRecord(false);
                setRecordSecond(false);
                setEnterNumber(false);
              }}
            >
              <AiOutlineRedo />
              Thực hiện lại
            </p>
          </div>
        </div>
      </div>
    </Container>
  );
}
const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  .content {
    display: flex;
    flex-direction: column;
    align-items: center;
    margin-bottom: 100px;
    text-align: center;
    padding: 15px;
    .spin__number {
      padding: 20px;
      max-width: 1000px;
      width: 100%;
      border-radius: 8px;
      background: #fff;
      color: #000;
      display: flex;
      flex-direction: column;
      .spin__main {
        display: flex;
        flex-direction: column;
        align-items: center;
        .title {
          display: flex;
          flex-direction: column;
          align-items: center;
          h3 {
            padding-top: 10px;
            color: #003e9c;
            font-weight: 550;
            font-size: 28px;
          }
        }
        .result {
          width: 150px;
        }
        .records {
          margin-top: 10px;
          img {
            height: 70px;
          }
        }
        .result__text {
          p {
            padding-top: 60px;
            color: #00ba88;
          }
        }
      }

      .roll__back {
        p {
          display: flex;
          gap: 6px;
          justify-content: flex-end;
          align-items: center;
          padding: 0 20px;
          color: #003e9c;
          cursor: pointer;
        }
      }
    }
    h1 {
      font-size: 80px;
      font-family: "Kinn";
      margin-bottom: 40px;
    }
    h3 {
      text-transform: uppercase;
      margin: 10px;
      font-weight: 400;
      letter-spacing: 2.2px;
    }
  }

  @media (max-width: 1365.98px) {
  }

  @media (max-width: 1199.98px) {
    .content .spin__number {
      max-width: 90%;
    }
  }

  @media (max-width: 991.98px) {
  }

  @media (max-width: 767.98px) {
    .logo__max {
      width: fit-content;
      margin: auto;
    }

    .list-voice__number {
      li {
        p:nth-child(1) {
          font-size: 25px !important;
        }
      }
    }

    .main__section {
      border-top: 1px solid !important;
    }
  }

  @media (max-width: 575.98px) {
    .content {
      .title-big {
        font-size: 40px !important;
      }
    }
  }

  @media (max-width: 375.98px) {
  }
`;

export default VerifyRecords;
