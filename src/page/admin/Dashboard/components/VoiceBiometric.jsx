import React, { useState } from "react";
import styled from "styled-components";
import voicebg from "../../../../assets/images/voicebg.png";
import logovntel from "../../../../assets/images/logo.png";
import logovntelMini from "../../../../assets/images/logoMini.png";
import EnterNumber from "./EnterNumber";
import EnterNumberSecond from "./EnterNumberSecond";
import CompareRecords from "./CompareRecords";
import VerifyRecords from "./VerifyRecords";
import { Link } from "react-router-dom";

function VoiceBiometric(props) {
  const [enterNumber, setEnterNumber] = useState(false);
  const [record, setRecord] = useState(false);
  const [recordSecond, setRecordSecond] = useState(false);
  const [verify, setVerify] = useState(false);

  console.log("record", record);

  return (
    <Container img={voicebg}>
      <div className="main">
        <div className="logo__max">
          <img src={logovntel} alt="" />
        </div>
        <div className="log__min">
          <p>Power by</p>
          <img src={logovntelMini} alt="" />
        </div>
        {enterNumber ? (
          record ? (
            verify ? (
              <VerifyRecords
                setRecord={setRecord}
                setRecordSecond={setRecordSecond}
                setEnterNumber={setEnterNumber}
              />
            ) : (
              <CompareRecords
                setVerify={setVerify}
                setRecord={setRecord}
                setRecordSecond={setRecordSecond}
              />
            )
          ) : recordSecond ? (
            <EnterNumberSecond setRecord={setRecord} setVerify={setVerify} />
          ) : (
            <EnterNumber setRecordSecond={setRecordSecond} />
          )
        ) : (
          <div className="voice__container">
            <div className="link__back">
              {" "}
              <Link
                to="/"
                style={{
                  paddingTop: 60,
                  marginBottom: 50,
                  display: "flex",
                  width: "fit-content",
                  height: "fit-content",
                  marginBottom: 30,
                  color: '#FFFFFF'
                }}
              >
                Quay lại
              </Link>
            </div>
            <div className="content">
              <h1 style={{ fontSize: 106, marginBottom: 10, fontWeight: 600 }} className="title-big">
                Voice Biometrics
              </h1>
              <h3>Giải pháp sinh trắc học giọng nói</h3>
              <div className="btn__start">
                <div onClick={() => setEnterNumber(true)}>Bắt đầu</div>
              </div>
            </div>
          </div>
        )}
      </div>
    </Container>
  );
}
const Container = styled.div`
  .main {
    background-image: url(${(props) => props.img});
    min-height: 100vh;
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
    position: relative;
    color: #fff;
    text-align: center;
    .logo__max {
      padding: 20px 0 0 40px;
    }
    .log__min {
      position: absolute;
      bottom: 20px;
      width: 100%;
      display: flex;
      align-items: center;
      gap: 10px;
      justify-content: center;
      p {
        margin: 0;
      }
    }
    .voice__container {
      display: flex;
      flex-direction: column;
      padding: 10px;
      .link__back {
        display: flex;
        justify-content: flex-end;
        padding: 10px 20px;
        text-decoration: underline;
        cursor: pointer;
      }
      .content {
        display: flex;
        flex-direction: column;
        align-items: center;
        margin-bottom: 100px;
        h1 {
          font-size: 80px;
          margin-bottom: 40px;
          /* font-family: "Kinn"; */
        }
        h3 {
          text-transform: uppercase;
          margin: 10px;
          font-weight: 400;
          letter-spacing: 2.2px;
        }
        .btn__start {
          margin: 20px 0 0;
          width: 240px;
          background: #e6f0ff !important;
          display: flex;
          justify-content: center;
          padding: 20px;
          border-radius: 100px;
          cursor: pointer;
          div {
            color: #003e9c;
            font-size: 20px;
          }
        }
      }
    }
  }

  @media (max-width: 1365.98px) {
  }

  @media (max-width: 1199.98px) {
    .content .spin__number {
      max-width: 90%;
    }
  }

  @media (max-width: 991.98px) {
  }

  @media (max-width: 767.98px) {
    .logo__max {
      width: fit-content;
      margin: auto;
    }

    .list-voice__number {
      li {
        p:nth-child(1) {
          font-size: 25px !important;
        }
      }
    }

    .btn__start {
      padding: 10px 20px !important;
      width: fit-content !important;
    }

    .hxkSuk .content .spin__number .spin__main .main__content .title__header {
      display: none !important;
    }

  }

  @media (max-width: 575.98px) {
    .content {
      .title-big {
        font-size: 40px !important;
      }
    }
  }

  @media (max-width: 375.98px) {
  }

`;

export default VoiceBiometric;
