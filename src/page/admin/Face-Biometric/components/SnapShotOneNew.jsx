import React, { useEffect, useState, useRef } from "react";
import styled from "styled-components";
import btnVoice from "../../../../assets/images/btn-take-photo.png";
import paternImg from "../../../../assets/images/img-photo.jpg";
import { BsFillArrowRightCircleFill } from "react-icons/bs";
import logovntelMini from "../../../../assets/images/logoMini.png";
import { useDispatch, useSelector } from "react-redux";
import { addImgOne } from "../../../../features/VerifyFace";
import { HiArrowPath } from "react-icons/hi2";
import Webcam from "react-webcam";
import html2canvas from "html2canvas";
import Toast from "../../../../components/common/Toast";
import {
  showMessageSuccess,
  showMessageError,
} from "../../../../features/AlertSlice";
import WebcamDemo from "../../ekyc/WebcamDemo";
const FACING_MODE_USER = "user";
const FACING_MODE_ENVIRONMENT = "environment";

function SnapShotOneNew({ setContinueImg2, setClassAdd }) {
  // const img1 = useSelector((state) => state.verifyFace.img1);
  const dispatch = useDispatch();
  const [camera, setCamera] = useState(false);
  const [captureImg, setCaptureImg] = useState(false);
  // ======================
  const webcamRef = useRef(null);
  const [isStreaming, setIsStreaming] = useState(false);
  const [imgSrc, setImgSrc] = useState(null);
  const videoConstraints = {
    facingMode: FACING_MODE_USER,
  };
  const [facingMode, setFacingMode] = React.useState(FACING_MODE_USER);
  const handleClick = React.useCallback(() => {
    setFacingMode((prevState) =>
      prevState === FACING_MODE_USER
        ? FACING_MODE_ENVIRONMENT
        : FACING_MODE_USER
    );
  }, []);
  const handleStartStream = () => {
    setIsStreaming(true);
    setCamera(true);
  };

  const handleStopStream = () => {
    setIsStreaming(false);
  };
  const handleTakePhoto = () => {
    const imgData = webcamRef.current.getScreenshot();
    setImgSrc(imgData);
  };

  useEffect(() => {
    if (imgSrc) {
      dispatch(addImgOne(imgSrc));
      setCaptureImg(true);
      // alert("chup anh thanh cong ");
      dispatch(showMessageSuccess());
    }
    setClassAdd(true);
  }, [imgSrc]);

  return (
    <>
      <Container>
        <div className="content">
          <h1 className="title-big">Face Matching</h1>
          <h3>Hệ thống nhận dạng khuôn mặt</h3>
          <div className="spin__number">
            <div className="spin__main">
              <div className="title">
                <h3>Chụp ảnh lần 1</h3>
                <p>Giữ khuôn mặt nằm trong vùng xanh</p>
              </div>
              <div className="patern__img">
                {isStreaming ? (
                  <div
                    id="webcam-container"
                    style={{
                      width: "460px",
                      height: "240px",
                      zIndex: "1000",
                    }}
                  >
                    <WebcamDemo setImgSrc={setImgSrc} setCaptureImg={setCaptureImg} />
                  </div>
                ) : (
                  <img src={paternImg} alt="" />
                )}
              </div>
              <div className="records">
              {!isStreaming && (
                <img
                  onClick={() => {
                    // start();
                    handleStartStream();
                  }}
                  src={btnVoice}
                  alt=""
                /> ) }
                {/* {captureImg ? (
                  <>
                    <div onClick={handleTakePhoto} className="capture">
                      <HiArrowPath /> Chụp lại
                    </div>
                    <div onClick={handleClick} className="capture">
                      Xoay
                    </div>
                  </>
                ) : (
                  <>
                    <div onClick={handleTakePhoto} className="capture">
                      Chụp
                    </div>
                    <div onClick={handleClick} className="capture">
                      Xoay
                    </div>
                  </>
                )} */}
              </div>
            </div>
            {captureImg && (
              <div className="continue">
                <div
                  onClick={() => {
                    setContinueImg2(true);
                  }}
                  className="btn__continue"
                >
                  Tiếp tục <BsFillArrowRightCircleFill />
                </div>
              </div>
            )}
            <div className="toast">
              <Toast />
            </div>
          </div>
          <div className="footer">
            <p>Power by</p>
            <img src={logovntelMini} alt="" />
          </div>
        </div>
      </Container>
    </>
  );
}
const wait = (time) => new Promise((resolve) => setTimeout(resolve, time));
const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;

  .footer {
    margin: 20px 0;
    width: 100%;
    display: flex;
    align-items: center;
    gap: 10px;
    justify-content: center;
    p {
      margin-top: 10px;
      margin-bottom: 0;
    }
  }
  .content {
    display: flex;
    flex-direction: column;
    align-items: center;
    text-align: center;
    width: 100%;
    .spin__number {
      max-width: 1000px;
      width: 100%;
      height: 500px;
      border-radius: 8px;
      background: #fff;
      color: #000;
      display: flex;
      flex-direction: column;
      position: relative;
      #webcam-container {
        video {
          width: 100% !important;
          height: 100% !important;
        }
      }
      .continue {
        position: absolute;
        right: 20px;
        bottom: 40px;
      }
      .main__video {
        position: absolute;
        top: 100px;
        /* width: 100%; */
        left: 280px;
      }
      .spin__main {
        display: flex;
        flex-direction: column;
        align-items: center;

        .title {
          display: flex;
          flex-direction: column;
          align-items: center;
          h3 {
            color: #003e9c;
            font-weight: 500;
            font-size: 28px;
          }
        }
        .patern__img {
          img {
            height: 260px;
            object-fit: contain;
          }
        }
        .records {
          position: absolute;
          bottom: 20px;
          margin-top: 10px;
          cursor: pointer;
          display: flex;
          gap: 20px;
          align-items: center;
          .capture {
            padding: 10px 12px;
            background: #146c94;
            color: white;
            border-radius: 4px;
            div {
              display: flex;
              align-items: center;
            }
          }
          img {
            cursor: pointer;
            height: 70px;
          }
        }
      }

      .btn__continue {
        display: flex;
        gap: 10px;
        justify-content: flex-end;
        align-items: center;
        padding: 0 20px;
        color: #003e9c;
        cursor: pointer;
      }
    }
    h1 {
      font-size: 80px;
      line-height: 100px;
      font-family: "Kinn";
    }
    h3 {
      text-transform: uppercase;
      margin: 10px;
      font-weight: 400;
      letter-spacing: 2.2px;
    }
  }

  @media (max-width: 1365.98px) {
  }

  @media (max-width: 1199.98px) {
    .content .spin__number {
      max-width: 90%;
    }
  }

  @media (max-width: 991.98px) {
    .patern__img {
      > div {
        width: 100% !important;
      }
    }

    .spin__number .spin__main .records {
      gap: 10px !important;
    }
  }

  @media (max-width: 767.98px) {
    .logo__max {
      width: fit-content;
      margin: auto;
    }
  }

  @media (max-width: 575.98px) {
    .content {
      .title-big {
        font-size: 40px !important;
      }
    }
  }

  @media (max-width: 575.98px) {
    .continue {
      position: absolute;
      width: 60%;
      display: flex;
      justify-content: center;
      bottom: 10px !important;
    }
    .records {
      bottom: 30px !important;
    }
  }
  @media (max-width: 375.98px) {
    /* .spin__main {
      width: 288px !important;
    } */
  }
`;
const StyleBorder = styled.div`
  position: absolute;
  top: 96px;
  left: 276px;
  height: 240px;
  width: 460px;
  /* border-radius: 50%; */
  background: transparent;
  border: 4px solid green;
  z-index: 1000;
`;
const StyleBorderErr = styled.div`
  position: absolute;
  top: 96px;
  left: 276px;
  height: 240px;
  width: 460px;
  /* border-radius: 50%; */
  background: transparent;
  border: 4px solid red;
  z-index: 1000;
`;

export default SnapShotOneNew;
